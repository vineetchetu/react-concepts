import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import App from '../App'
import Login from '../component/Login/Login'
import Signup from '../component/Signup/'

const Routes = ()=>(
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={App}/>
            <Route exact path='/login' component={Login}/>
            <Route exact path='/sign-up' component={Signup}/>            
        </Switch>
    </BrowserRouter>
)
export default Routes;