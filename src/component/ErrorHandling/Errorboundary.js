import React, { Component } from 'react';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true });
    // You can also log the error to an error reporting service
    console.log(error, info);
  }

  render() {
	  let view = '';
	  if (this.state.hasError) {
		  view = <h2>Something went wrong.</h2>
	  } else {
		  view = <h2>Something all correct.</h2>
	  }
	  return(
		<div>
			{view}
		</div>
	)
  }
}

export default ErrorBoundary