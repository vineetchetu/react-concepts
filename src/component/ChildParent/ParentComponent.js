import React from 'react';
import ChildComponent from './ChildComponent';
var obj;
 
class ParentComponent extends React.Component {
    constructor(props){
        super(props);
        this.onTextChange = this.onTextChange.bind(this);
        this.state = {
            val: 0
        };
		//obj = {name:'Vineet',desig:'Engineer'}
		obj = 5
    }
 
    onTextChange(val) {
        var newVal = val + this.state.val;
        this.setState({val: newVal});
    }	
	
    render() {
        return (
            <container-wrapper>
                <ChildComponent onTextChange1={this.onTextChange} val={this.state.val} data={obj} />
                <h1>Parent Component Input Value {this.state.val}</h1>
            </container-wrapper>
        );
    }
}
 
export default ParentComponent