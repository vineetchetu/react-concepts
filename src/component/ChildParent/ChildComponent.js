import React from 'react';
import PropTypes from 'prop-types';
 
class ChildComponent extends React.Component {
    constructor(props){
        super(props);
        this.onTextChange = this.onTextChange.bind(this);
    }
 
    onTextChange(e) {
        var val = e.target.value; // You can pass value as per your requirement 
        this.props.onTextChange1(val); // onTextChange(val) is ParentComponent class function
    }
    render() {
		console.log('Object props types are',typeof(this.props.data))
		console.log('Object props data are',this.props.data)
        return (
            <container>
                <input type="text" onChange={this.onTextChange} />
                <h2>Child Component Input Will Be Value {this.props.val} + 5</h2>
            </container>
        );
    }
}
ChildComponent.propTypes = {
  data: PropTypes.string
};
    
export default ChildComponent