import React from 'react';    
import PropTypes from 'prop-types';  
  
class Child extends React.PureComponent {    
    constructor(props) {  
      super(props);
   } 
    
   render() {  
       console.log('Child.js Component render '+Date.now());  
      return (  
          <div>    
          <p>Props Values is {this.props.childProps}</p>
         </div>  
      );    
   }  
     
}    
Child.propTypes ={  
    childProps:PropTypes.array.isRequired,  
   }    
    
export default Child;