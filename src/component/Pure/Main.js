import React from 'react';    
import PropTypes from 'prop-types';  
import Child from './Child';  
class Main extends React.Component {    
    constructor(props) {  
      super(props);          
      this.state = { 
        name:'Pankaj',
         city: "Alwar",
         data:[Date.now()]  
      }  
   } 

   addData(){
       var arrayData=this.state.data;
       arrayData.push(Date.now());
       this.setState({data:arrayData});
	   //this.setState({data:new Array(arrayData)});
   }
   
   render() {  
       console.log('Main Component render');  
      return (  
          <div>        
         <p>User Name: {this.state.name}</p>  
         <p>User City: {this.state.city}</p>
         <p>User Data: {this.state.data.join("  ,")}</p> 
         <Child childProps={this.state.data}></Child>
         <input type="button" value="UpdateData" onClick={this.addData.bind(this)} /> 
         </div>  
      );    
   }       
}    
 export default Main;