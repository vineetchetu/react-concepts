import React from 'react';
import Sibling1 from './Sibling1';
import Sibling2 from './Sibling2';
 
class Main extends React.Component {
    constructor(props){
        super(props);    
		this.state = { newVal : ''	}
    }

    getTextValue = (val) => {
        console.log('Text val are ', val)
		this.setState({newVal:val})
    }
	
    render() {
        return (
             <div>
				<Sibling1 getval={this.getTextValue} />
				<Sibling2 getNewVal={this.state.newVal} />
			</div>
        );
    }
}
 
export default Main