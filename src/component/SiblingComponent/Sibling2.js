import React from 'react';
 
class Sibling2 extends React.Component {
    constructor(props){
        super(props);               
    }
	
	componentWillReceiveProps(nextProps){
		console.log('prevProps is ',this.props)
		console.log('nextProps is ',nextProps)
	}
	
	onClick = () =>{
		alert(this.props.getNewVal)		
	}
	
	
    render() {
        return (
             <div>
				<input type='button' name='fname' Value='Submit' onClick={this.onClick}/>
			</div>
        );
    }
}
 
export default Sibling2