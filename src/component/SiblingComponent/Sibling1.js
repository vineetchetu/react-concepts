import React from 'react';
 
class Sibling1 extends React.Component {
    constructor(props){
        super(props);
        this.onTextChange = this.onTextChange.bind(this);
        this.state = {
            val: ''
        };
    }
    onTextChange(e) {        
        this.setState({val: e.target.value});
		this.props.getval(e.target.value)
    }
    render() {
        return (
            <div>
				<input type='text' name='fname' value={this.state.val} onChange={this.onTextChange} />
			</div>
        );
    }
}
 
export default Sibling1