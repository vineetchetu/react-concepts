import React, { Component } from 'react';

class SearchBar extends Component{
	constructor(){
		super()
        this.state = {           
			query:'',			
        };
	}

  handleChange = (event) => {
    this.setState({query: event.target.value}, () => {
		this.props.onFilter(this.state.query)
	});
  }
    
  render() {
	  {this.state.query}
	return (
		<input type="search" placeholder="Filter Results" onChange={this.handleChange} value={this.state.query} />
	)
  }
}

class ProductRow extends Component {
	constructor(props){
		super(props)   
		console.log('item name is ',this.props.name)		
	}
	render() {		
		return (
			<li className="list-item">{this.props.name}</li>
		)
	}
}

class Application extends Component {	
	constructor(props){
		super(props)
        this.state = {           
			items: this.props.items || []		
        };
	}

	filter = (query) => {		
		const regex = new RegExp(query, 'i');
		const filteredItems = this.props.items.filter((item) => {
			return regex.test(item.name);
		})
		this.setState({items: filteredItems})
	}

	render() {
		console.log('items are ', this.state.items)
		//attItems = this.state.items
		return (			
			<div>				
				<SearchBar onFilter={this.filter} />
				<ul className='list'>
					<li>Good</li>
					{
						this.state.items.map((item) => 														
							<ProductRow name={item.name} />
						)						
					}
				</ul>
			</div>
		);
	}
}
export default Application;