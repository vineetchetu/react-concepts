import React, { Component } from 'react'
export default function ourFirstStore(WrappedComponent) {
    return class extends Component {
        state = {
            username: 'Vineet',
        };

        render() {
            return <WrappedComponent username={this.state.username} {...this.props} />;
        }
    };
}