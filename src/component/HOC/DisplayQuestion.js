import React, {Component} from 'react';
import ourFirstStore from "./ourFirstStore";

class DisplayQuestion extends Component {
    render() {
        return (
            <div>
                What do you want to order today, {this.props.username} ?
            </div>
        );
    }
}


export default ourFirstStore(DisplayQuestion);