import React, { Component } from 'react';

function WrapProduct(WrapComponent, request){
	class WrapProduct extends Component{
		constructor(props){
			super(props);
			this.state = { productData : null}
		}
		componentDidMount() {			
			var datalist = this.getdata(request)
			this.setState({
			  productData:datalist
			});							
		}
		
		getdata(url){
			return fetch(url).then(data => {
				return data.json()
			});			
		}

		render() {
		  return <WrapComponent response={this.state.productData} {...this.props} />;
		}
	}

	return WrapProduct;
}
export default WrapProduct;
