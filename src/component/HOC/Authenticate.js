import React from 'react';

function Authenticate(WrappedComponent) {
    class Authenticate extends React.Component {
        constructor(props) {
            super(props);
            this.state = {};
        }

        onInputChange = (event) => {
            event.preventDefault();
            const inputName = event.target.name;
            const inputValue = event.target.value;
            this.setState({ [inputName]: inputValue });
        }

        submit = () => {
            console.log(this.state)
        }

        render() {
            const { onSubmit, ...otherProps } = this.props;
            return (<WrappedComponent
            onChange={this.onInputChange}
            onSubmit={this.submit}
            {...otherProps}
            />);
        }
    }

    return Authenticate;
}

export default Authenticate;