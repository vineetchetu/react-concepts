import React, {Component} from 'react';
import ourFirstStore from "./ourFirstStore";

class DisplayHello extends Component {
    render() {
        return (
            <p>
                Hello {this.props.username} !
            </p>
        );
    }
}


export default ourFirstStore(DisplayHello);