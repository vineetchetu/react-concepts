import React from 'react'

function SignUp(props) {
    const {onSubmit, onChange, showSignIn} = props
    return (
    <div className="box-container">
        <form onSubmit={onSubmit}>
            <div className="title"> Sign Up </div>
            <label htmlFor="email"> Email </label>
            <input
                id="email"
                type="email"
                name="email"
                onChange={onChange}
                className="form-input"
            />
            <label htmlFor="username"> Username </label>
            <input
                id="username"
                type="name"
                name="name"
                onChange={onChange}
                className="form-input"
            />
            <label htmlFor="password"> Password </label>
            <input
                id="password"
                type="password"
                name="password"
                onChange={onChange}
                className="form-input"
            />
            <label htmlFor="name"> Name </label>
            <input
                id="name"
                type="name"
                name="name"
                onChange={onChange}
                className="form-input"
            />
            <button type="submit" name="action" className="button">Submit</button>
        < /form>

        <div className="link"> Already have an account ?
            <a href="#!" onClick={showSignIn} className="switch"> SignIn </a>
        < /div>
    < /div>
)
}

export default SignUp;