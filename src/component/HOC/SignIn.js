import React from 'react';

function SignIn(props) {
    const {onChange, onSubmit, showSignup } = props;
    return (
    <div className="box-container">
        <form onSubmit={onSubmit}>
            <div className="title"> Sign In </div>
            <label htmlFor="email">Email</label>
            <input
                id="email"
                type="email"
                name="email"
                onChange={onChange}
                className="form-input"
            />
            <label htmlFor="password">Password</label>
            <input
                id="password"
                type="password"
                name="password"
                onChange={onChange}
                className="form-input"
            />
            <button type="submit" name="action" className="button">Submit</button>
        </form>
        <div className="link"> Do not have an account?
            <a href="#!" onClick={showSignup} className="switch">SignUp</a>
        </div>
    </div>
);
}

export default SignIn