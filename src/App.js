import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import {Link} from 'react-router-dom'

import Abc from './component/Login/Login'
import Main from './component/Pure/Main'  //Pure component
import ParentComponent from './component/ChildParent/ParentComponent'  //Pure component
import ForceUpdate from './ForceUpdate'
import SetState from './SetState'
import PrevState from './PrevState'
import Inheritance from './component/Inheritance'
import Composition from './component/Composition'
import SignIn from './component/HOC/SignIn';
import SignUp from './component/HOC/SignUp';
import Authenticate from './component/HOC/Authenticate';
import Productlist from './component/HOC/Productlist';
import Producttable from './component/HOC/Producttable';
import WrapProduct from './component/HOC/Wrapproduct';
import DisplayHello from './component/HOC/DisplayHello';
import DisplayQuestion from './component/HOC/DisplayQuestion';
import Errorboundary from './component/ErrorHandling/Errorboundary';
import MainSibling from './component/SiblingComponent/Main';
import Interview from './component/Interview';

const SignUpForm = Authenticate(SignUp);
const SignInForm = Authenticate(SignIn);


const ListProduct = WrapProduct(Productlist, 'https://jsonplaceholder.typicode.com/todos/1');
const TableProduct = WrapProduct(Producttable,'https://jsonplaceholder.typicode.com/todos/2');



class App extends Component {
	constructor() {
        super()
        this.state = {           
			name:'Prem Verma',
			userAccess: true,
        };
		console.log('constructor');
		
	
	}
	
	componentWillMount(){
		console.log('componentWillMount');
	}
	
	componentDidMount(){
		console.log('componentDidMount');
	}
	
	
	componentWillReceiveProps(nextProps){
		console.log('componentWillReceiveProps	');
	}
	
	shouldComponentUpdate(nextProps, nextState){
		console.log('shouldComponentUpdate	');
		if(nextState.name !== this.state.name){
			return true
		} else {
			return false;
		}
		//this.forceUpdate();
		//return true;		
	}
	
	componentWillUpdate(nextProps, nextState){
		console.log('componentWillUpdate	');
		console.log('next props',nextProps);
		console.log('next state', nextState);
	}
	
	componentDidUpdate(prevProps, prevState){
		
		console.log('componentDidUpdate	');
		console.log('prev props',prevProps);
		console.log('prev state', prevState);
	}
	
	
	componentWillUnmount(){
		console.log('component will unmount')
	}
	
	onclick = () => {
		this.setState({name: 'Ankit Tomar'})
		this.setState({name: 'Shibu Tomar'})
		console.log('State name is ', this.state.name)
	}
	
	toggleForm = () => {
		alert('HI')
        this.setState({
            userAccess: !this.state.userAccess
        });
    }
	
	render() {
		console.log('render');
		const { signInUser, signUpUser } = this.props;
		const data = [
		  {name: "Football"},
		  {name: "Baseball"},
		  {name: "Bascketball"},
		  {name: "Cricket"},
		  {name: "Rugby"},
		  {name: "Surfing"}
		]
		
		return (
		  <div className="App"> 
		  |Parent Name : {this.state.name}   <br /><br />
		|Child Name : <Abc name={this.state.name} />
		
		<button onClick={this.onclick}> Change state </button>
			<h3>----Force Update--------</h3>
			<ForceUpdate>
				<h1>This is props children</h1>
			</ForceUpdate>
			<SetState />
			<PrevState />
			<Link to="/sign-up">SignUP</Link>
			<Link to="/Login">Login</Link>
			<h3>----Pure Component--------</h3>
			<Main />
			<h3>----Child to Parent Component--------</h3>
			<ParentComponent />
			<h3>----Inheritance--------</h3>
			<Inheritance>
				<h1>Hi Children</h1>
			</Inheritance>
			<h3>----Composition--------</h3>
			<Composition />
			<h3>----High Order Function--------</h3>
				<div className="container">
					{
					this.state.userAccess ?
					(<SignInForm onSubmit={signInUser} showSignup={this.toggleForm} />) :
					(<SignUpForm onSubmit={signUpUser} showSignIn={this.toggleForm} />)
					}
				</div>
			<h3>----Component Did Catch--------</h3>
			<Errorboundary />
			<ListProduct />
			<TableProduct />
			<DisplayHello />
			<DisplayQuestion  />
			<h2>Sibling Communication</h2>
			<MainSibling />
			<h2>Interview Chaurasia</h2>
			<Interview items={data}/>
			</div>
		)				
	}
}

export default App;
